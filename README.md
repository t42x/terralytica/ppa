# t42x PPA

## Ubuntu Focal

### Install Repo

```sh
curl -s --compressed "https://apt.terralytica.t42x.io/KEY.gpg" | sudo apt-key add -

sudo curl -s --compressed -o /etc/apt/sources.list.d/t42x.list "https://apt.terralytica.t42x.io/ubuntu/focal/t42x.io.list"

sudo apt update

sudo apt install kdms provctl
```
